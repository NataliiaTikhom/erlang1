-module(proc_sieve).

-export([generate/1]).
-export([gen_print/1]).
-export([sieve/0]).


% Функция для вычисления простых чисел до MaxN
gen_print(MaxN) ->
	lists:foreach(fun(X)->io:format("~w ",[X]) end, generate(MaxN)).


% возвращает список всех простых чисел от 2 до MaxN, включительно
generate(MaxN) when MaxN > 1 ->
	PID = spawn(proc_sieve, sieve, []),
	generate_sup(2, MaxN, PID);
generate(_) ->
	[].


% вспомогательные функции
generate_sup(CurNum, MaxN, PID) when CurNum =< MaxN ->
	PID ! CurNum,
	generate_sup(CurNum + 1, MaxN, PID);
generate_sup(_, _, PID) ->
	PID ! {done, self()},
	receive
		{result, List} ->
			List;
		_ ->
			{error, "Wrong output"}
	end.


% функция "фильтрует" числа делящиеся на N
sieve() ->
	receive
		FilterBy ->
			sieve_sup(FilterBy, nil, nil)
	end.


% вспомогательная функция
sieve_sup(Filter, RedirectPid, SavedBackPID) ->
	receive
		{done, BackPID} ->
			if
				RedirectPid == nil ->
					BackPID ! {result, [Filter]};
				true ->
					RedirectPid ! {done, self()},
					sieve_sup(Filter, nil, BackPID)
			end;
		{result, List} ->
			if
				SavedBackPID =/= nil ->
						SavedBackPID ! {result, [Filter | List]};
				true ->
					nil
			end;
		CurNum when (CurNum rem Filter) =/= 0 ->
			if
				RedirectPid == nil ->
					PID = spawn(proc_sieve, sieve, []),
					PID ! CurNum,
					sieve_sup(Filter, PID, SavedBackPID);
				true ->
					RedirectPid ! CurNum,
					sieve_sup(Filter, RedirectPid, SavedBackPID)
			end;
		_ ->
			sieve_sup(Filter, RedirectPid, SavedBackPID)
	end.
