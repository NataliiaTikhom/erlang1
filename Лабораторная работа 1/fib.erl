-module(fib).

% экспортируем три функции, у каждой по одному аргументу
-export([fib_p/1]).
-export([fib_g/1]).
-export([tail_fib/1]).

% не используем хвостовую рекурсию
% рассмотрим два конечных случая
fib_p(0) ->
    0;
fib_p(1) ->
    1;

% общий случай
fib_p(N) ->
    fib_p(N - 1) + fib_p(N - 2).


% с помощью сторожевых последовательностей (веток when)
fib_g(N) when N > 1 ->
    fib_g(N - 1) + fib_g(N - 2);

fib_g(N) ->
	N.


% с хвостовой рекурсией
tail_fib(N) ->
	tail_fib_sup(N, 0 ,1).

% вспомогательные функции
tail_fib_sup(0, PrevResult, _) ->
	PrevResult;
tail_fib_sup(N, PrevResult, CurResult) ->
	tail_fib_sup(N - 1, CurResult, PrevResult + CurResult).
