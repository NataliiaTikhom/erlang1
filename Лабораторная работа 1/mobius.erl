-module(mobius).

% экспортируем 4 функции
-export([is_prime/1]).
-export([prime_factors/1]).
-export([is_square_multiple/1]).
-export([find_square_multiples/2]).


% функция осуществляет проверку числа на то, простое ли оно
is_prime(N) ->
	is_prime_sup(2, N).

% вспомогательные функции
is_prime_sup(Divisor, N) when (Divisor * Divisor) =< N ->
	if
		(N rem Divisor) == 0 ->
			false;
		true ->
			is_prime_sup(Divisor + 1, N)
	end;
is_prime_sup(_, _) -> true.



% возвращает список простых сомножителей N
prime_factors(0) ->
	[];
prime_factors(N) ->
	prime_factors_sup(N, 2, [1]).

% вспомогательные функции
prime_factors_sup(N, CoMultiplier, CurResult) when CoMultiplier =< N ->
	if
		(N rem CoMultiplier) == 0 ->
			IsPrime = is_prime(CoMultiplier),
			if 
				IsPrime == true ->
					prime_factors_sup(N, CoMultiplier + 1, [CoMultiplier|CurResult]);
				true ->
					prime_factors_sup(N, CoMultiplier + 1, CurResult)
			end;
		true ->
			prime_factors_sup(N, CoMultiplier + 1, CurResult)
	end;
prime_factors_sup(_, _, Result) ->
	Result.


% возвращает true, если аргумент делится на квадрат простого числа
% возвращает false, если не делится
is_square_multiple(N) when N < 4 ->
	false;  % тут сразу видно, что нет
is_square_multiple(N) ->
	is_square_multiple_sup(N, 2).  % тут вызываем вспомогательную функцию

% вспомогательные функции
is_square_multiple_sup(N, CoMultiplier) when CoMultiplier =< N ->
	if
		(N rem CoMultiplier) == 0 ->
			IsPrime = is_prime(CoMultiplier),
			if 
				IsPrime == true ->
					if
						(N rem (CoMultiplier * CoMultiplier)) == 0 ->
							true;
						true ->
							is_square_multiple_sup(N, CoMultiplier + 1)
					end;
				true ->
					is_square_multiple_sup(N, CoMultiplier + 1)
			end;
		true ->
			is_square_multiple_sup(N, CoMultiplier + 1)
	end;
is_square_multiple_sup(_, _) ->
	false.


% Эта функция получает Count - длину последовательности чисел 
% делящихся на квадрат простого числа, и MaxN - максимальное значение, 
% после которого надо прекратить поиск.
find_square_multiples(Count, MaxN) ->
	find_square_multiples_sup(0, failure, 0, Count, MaxN).

% вспомогательные функции
find_square_multiples_sup(CurNum, FirstInLine, FoundNum, NeedFind, SearchLimit) 
when CurNum =< SearchLimit , FoundNum < NeedFind ->
	IsSuitable = is_square_multiple(CurNum),
	if
		IsSuitable == true ->
			if
				FirstInLine == failure ->
					find_square_multiples_sup(CurNum + 1, CurNum, FoundNum + 1, NeedFind, SearchLimit);
				true ->
					find_square_multiples_sup(CurNum + 1, FirstInLine, FoundNum + 1, NeedFind, SearchLimit)
			end;
		true ->
			find_square_multiples_sup(CurNum + 1, failure, 0, NeedFind, SearchLimit)
	end;
find_square_multiples_sup(_, FirstInLine, FoundNum, NeedFind, _) when FoundNum == NeedFind ->
	FirstInLine;
find_square_multiples_sup(_, _, _, _, _) ->
	failure.

